package com.epam.gateway.gatewayservice.controllers;

import com.epam.gateway.gatewayservice.feignclients.BackendServiceFeignClient;
import com.epam.gateway.gatewayservice.serviceapi.BackendService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class BackendController implements BackendService {

    private final BackendServiceFeignClient backendServiceFeignClient;

    public BackendController(BackendServiceFeignClient backendServiceFeignClient) {
        this.backendServiceFeignClient= backendServiceFeignClient;
    }


    @Override
    public ResponseEntity<String> indexProduct() {
        return backendServiceFeignClient.indexProduct();
    }

    @Override
    public ResponseEntity<String> createProduct(String product) {
        return backendServiceFeignClient.createProduct(product);
    }

    @Override
    public ResponseEntity<String> viewProduct(long id) {
        return backendServiceFeignClient.viewProduct(id);
    }

    @Override
    public ResponseEntity<String> editProduct(long id, String product) {
        return backendServiceFeignClient.editProduct(id, product);
    }

    @Override
    public ResponseEntity<String> viewImagesProduct(String productId) {
        return backendServiceFeignClient.viewImagesProduct(productId);
    }

    @Override
    public ResponseEntity<String> serveFileProduct(String id) {
        return backendServiceFeignClient.serveFileProduct(id);
    }

    @Override
    public ResponseEntity<String> handleFileUploadProduct(String id, MultipartFile file) {
        return backendServiceFeignClient.handleFileUploadProduct(id, file);
    }

    @Override
    public ResponseEntity<String> indexGroup() {
        return backendServiceFeignClient.indexGroup();
    }

    @Override
    public ResponseEntity<String> createGroup(String group) {
        return backendServiceFeignClient.createGroup(group);
    }

    @Override
    public ResponseEntity<String> viewGroup(long id) {
        return backendServiceFeignClient.viewGroup(id);
    }

    @Override
    public ResponseEntity<String> editGroup(long id, String group) {
        return backendServiceFeignClient.editGroup(id,group);
    }

    @Override
    public ResponseEntity<String> getCartItems(String cartId) {
        return backendServiceFeignClient.getCartItems(cartId);
    }

    @Override
    public ResponseEntity<String> createCart() {
        return backendServiceFeignClient.createCart();
    }

    @Override
    public ResponseEntity<String> addProductToCart(String cartId, String cartItem) {
        return backendServiceFeignClient.addProductToCart(cartId, cartItem);
    }

    @Override
    public ResponseEntity<String> removeItemFromCart(String cartId, String productId) {
        return backendServiceFeignClient.removeItemFromCart(cartId, productId);
    }

    @Override
    public ResponseEntity<String> setProductQuantityInCart(String cartId, String cartItem) {
        return backendServiceFeignClient.setProductQuantityInCart(cartId, cartItem);
    }

    @Override
    public ResponseEntity<String> createOrderFromCart(String cartId, String order) {
        return backendServiceFeignClient.createOrderFromCart(cartId, order);
    }
}
