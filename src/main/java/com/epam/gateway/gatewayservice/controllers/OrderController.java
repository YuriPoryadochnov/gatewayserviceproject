package com.epam.gateway.gatewayservice.controllers;

import com.epam.gateway.gatewayservice.feignclients.OrderServiceFeignClient;
import com.epam.gateway.gatewayservice.serviceapi.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController implements OrderService {


    private final OrderServiceFeignClient orderServiceFeignClient;

    public OrderController(OrderServiceFeignClient orderServiceFeignClient) {
        this.orderServiceFeignClient = orderServiceFeignClient;
    }


    @Override
    public ResponseEntity<String> index() {
        return orderServiceFeignClient.index();
    }

    @Override
    public ResponseEntity<String> create(String order) {
        return orderServiceFeignClient.create(order);
    }

    @Override
    public ResponseEntity<String> view(long id) {
        return orderServiceFeignClient.view(id);
    }

    @Override
    public ResponseEntity<String> edit(long id, String order) {
        return orderServiceFeignClient.edit(id, order);
    }
}
