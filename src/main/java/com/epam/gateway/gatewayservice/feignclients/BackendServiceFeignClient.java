package com.epam.gateway.gatewayservice.feignclients;

import com.epam.gateway.gatewayservice.serviceapi.BackendService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "backend")
public interface BackendServiceFeignClient extends BackendService {
}
