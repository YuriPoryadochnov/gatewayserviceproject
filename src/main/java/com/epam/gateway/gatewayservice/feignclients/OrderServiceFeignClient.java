package com.epam.gateway.gatewayservice.feignclients;

import com.epam.gateway.gatewayservice.serviceapi.OrderService;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "order-service")
public interface OrderServiceFeignClient extends OrderService {
}
