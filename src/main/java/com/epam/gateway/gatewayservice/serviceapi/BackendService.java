package com.epam.gateway.gatewayservice.serviceapi;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


public interface BackendService {
    @RequestMapping(value = "/product", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> indexProduct();

    @RequestMapping(value = "/product", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> createProduct(@RequestBody String product);

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> viewProduct(@PathVariable("id") long id);

    @RequestMapping(value = "/product/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> editProduct(@PathVariable("id") long id, @RequestBody String product);

    @RequestMapping(value = "/product/{id}/images", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> viewImagesProduct(@PathVariable("id") String productId);

    @RequestMapping(value = "/product/image/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> serveFileProduct(@PathVariable("id") String id);

    @RequestMapping(value = "/product/{id}/uploadimage", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> handleFileUploadProduct(
            @PathVariable("id") String id, @RequestParam("file") MultipartFile file);

    @RequestMapping(value = "/group", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> indexGroup();

    @RequestMapping(value = "/group", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> createGroup(@RequestBody String group);

    @RequestMapping(value = "/group/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> viewGroup(@PathVariable("id") long id);

    @RequestMapping(value = "/group/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> editGroup(@PathVariable("id") long id, @RequestBody String group);

    @RequestMapping(value = "/cart/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getCartItems(@PathVariable("id") String cartId);

    @RequestMapping(value = "/cart", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> createCart();

    @RequestMapping(value = "/cart/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addProductToCart(@PathVariable("id") String cartId, @RequestBody String cartItem);

    @RequestMapping(value = "/cart/{id}/{product_id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeItemFromCart(@PathVariable("id") String cartId, @PathVariable("product_id") String productId);

    @RequestMapping(value = "/cart/{id}/quantity", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> setProductQuantityInCart(@PathVariable("id") String cartId, @RequestBody String cartItem);

    @RequestMapping(value = "/cart{id}/order", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> createOrderFromCart(@PathVariable("id") String cartId, @RequestBody String order);
}
