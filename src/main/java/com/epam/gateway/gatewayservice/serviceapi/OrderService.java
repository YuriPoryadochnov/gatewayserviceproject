package com.epam.gateway.gatewayservice.serviceapi;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface OrderService {
    @RequestMapping(value = "/order", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> index();

    @RequestMapping(value = "/order", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> create(@RequestBody String order);

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> view(@PathVariable("id") long id);

    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> edit(@PathVariable("id") long id, @RequestBody String order);
}
